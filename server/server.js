var conf = {
  debug: 1,
  sendMax: 2,
  requestTime: 5000
};

var http = require('http');
var sockjs = require('sockjs');
var geoip = require('geoip-lite');
var PeerServer = require('peer').PeerServer;

var app = {};

// global downloaded parts 
app._downloaded = {MN: {}, RW: {}};
app._request = {MN: {}, RW: {}};


app._connections = {};
app._hashes = {};

// callback
app._onConnection = function (conn) {
  var geo = geoip.lookup(conn.remoteAddress);

  // RW = Rest Of World
  // MN = Mongolia

  if (geo === null) {
    conn.country = 'MN';
  } else {
    conn.country = geo.country === 'MN' ? 'MN' : 'RW';
  }

  // connection
  app._connections[conn.id] = {'sent': 0, 'conn': conn};

  //
  conn.on('data', function (data) {
    try {
      var message = JSON.parse(data);
      if (['DOWNLOADED', 'RETREIVE'].indexOf(message.type) !== -1) {
        app._handleMessage(conn, message);
      } else {
        console.log('Message unrecognized');
      }
    } catch (e) {
      console.log(e);
      console.log('Invalid message', data);
      // throw e;
    }
  });

  // on connection close
  conn.on('close', function () {

    // delete from connection
    delete app._connections[conn.id];

    // delete app._downloaded[];
    for (var url in app._downloaded[conn.country]) {
      if (app._downloaded[conn.country][url].hasOwnProperty(conn.id)) {
        delete app._downloaded[conn.country][url][conn.id];
      }
    }

  });
};

app._sendRequest = function (country, url, peerId) {
  var message = JSON.stringify({
    type: 'REQUEST',
    peerId: peerId,
    url: url,
//    delete: false
  });

  // TODO round robin balancer
  for (var id in app._downloaded[country][url]) {
    if (!app._connections.hasOwnProperty(id)) {
      continue;
    }
    //
//    if ((app._connections[id]['sent'] + 1) >= conf.sendMax) {
//      message['delete'] = true;
//    }

    app._connections[id]['conn'].write(message);
    app._connections[id]['sent'] = app._connections[id]['sent'] + 1;

    var _conn = app._connections[id];

    // neg hereglegch neg (conf.sendMax) udaa uur hereglegch ruu segment yavuulna
    delete app._downloaded[country][url][id];

    // appending to last
    if (app._connections[id]['sent'] < conf.sendMax) {
      app._downloaded[country][url][id] = _conn;
    }

    break;
  }
};


app._handleMessage = function (conn, message) {
  switch (message.type) {
    case 'DOWNLOADED':
      var time = new Date().getTime() - conf.requestTime;

      // global parts
      if (!app._downloaded[conn.country].hasOwnProperty(message.url)) {
        app._downloaded[conn.country][message.url] = {};
      }
      if (!app._request[conn.country].hasOwnProperty(message.url)) {
        app._request[conn.country][message.url] = {};
      }

      // global hash info
      app._hashes[message.url] = { time: new Date().getTime() };

      // add to downloaded segment
      app._downloaded[conn.country][message.url][conn.id] = new Date().getTime();


      // uuriihuu requesting ustgane
      if (app._request[conn.country][message.url].hasOwnProperty(message.peerId)) {
        delete app._request[conn.country][message.url][message.peerId];
      }

      // uur hen negni request
      for (var peerId in app._request[conn.country][message.url]) {
        if (app._request[conn.country][message.url][peerId]['time'] > time) {
          // send request
          app._sendRequest(conn.country, message.url, peerId);

          delete app._request[conn.country][message.url][peerId]; // delete requester
          break;
        } else {
          delete app._request[conn.country][message.url][peerId];
        }
      }

      break;
    case 'RETREIVE':
      // downloaded
      if (!app._downloaded[conn.country].hasOwnProperty(message.url)) {
        app._downloaded[conn.country][message.url] = {};
      }
      // request
      if (!app._request[conn.country].hasOwnProperty(message.url)) {
        app._request[conn.country][message.url] = {};
      }

      // global hash info
      app._hashes[message.url] = {time: new Date().getTime() };

      // hen neg ni tatsan baival
      if (Object.keys(app._downloaded[conn.country][message.url]).length > 0) {
        app._sendRequest(conn.country, message.url, message.peerId);
      } else {
        // create request
        app._request[conn.country][message.url][message.peerId] = {id: conn.id, time: new Date().getTime()};
      }

      break;
  }
};


// Release memory
app._release = function () {
  var time = new Date().getTime() - 60 * 1000;

  // hashes
  for (var url in app._hashes) {
    if (app._hashes[url]['time'] < time) {
      delete app._hashes[url];

      // downloaded
      if (app._downloaded['MN'].hasOwnProperty(url)) {
        delete app._downloaded['MN'][url];
      }
      if (app._downloaded['RW'].hasOwnProperty(url)) {
        delete app._downloaded['RW'][url];
      }

      // request
      if (app._request['MN'].hasOwnProperty(url)) {
        delete app._request['MN'][url];
      }
      if (app._request['RW'].hasOwnProperty(url)) {
        delete app._request['RW'][url];
      }
    }
  }
};


// 
server = http.createServer();

// websocket
websocket = sockjs.createServer({});
websocket.on('connection', app._onConnection);

websocket.installHandlers(server, {
  prefix: '/sockjs'
});
server.listen(3000, '0.0.0.0');

// init peerserver
peerserver = PeerServer({port: 9000, path: '/peerjs'});

// release memory (10 second)
setInterval(app._release, 10 * 1000);


