'use strict';

var onair = {};
onair.log = {
    level: 0, // 1 warning only, 2 warning and info
    warn: function (msg) {
        if (onair.log.level >= 1) {
            console.warn(msg);
        }
    },
    info: function (msg) {
        if (onair.log.level >= 2) {
            console.info(msg);
        }
    }
};
onair.peer = (function () {
    var portSockjs = '3000';
    var portPeerjs = '9000';
    var hostname = location.hostname;

//    var maxWaitTimeout = 5000; // file receive max timeout
//    var minWaitTimeout = 3000; // file receive min timeout

    var serverReceivedBandwith = 0;
    var peerReceivedBandwith = 0;
    var peerSentBandwith = 0;


    var peerJsInitaliazed = false;
    var sockJsInitaliazed = false;

// downloaded segments 
    var downloaded = {};
    var callbacks = {};
    var fileReceiveTimeouts = {};
    var urls = {};

    // init peerjs
    var peerId = Math.floor(Math.random() * (99999999 - 10000000)) + 10000000;
    var peer = new Peer(peerId, {host: hostname, port: portPeerjs, path: '/peerjs'});
    onair.log.info("Peer Id:" + peerId);

    // файл хэн нэгнээс ирэх үед
    peer.on('connection', function (conn) {
        conn.on('data', function (data) {
            // trigger downloaded event
            onair.peer.downloaded(data.url, data.segment.slice(0), true);

            onair.log.info("Event part downloaded from peer: " + data.url);

            if (fileReceiveTimeouts.hasOwnProperty(data.url)) {
                window.clearTimeout(fileReceiveTimeouts[data.url]);
                delete fileReceiveTimeouts[data.url];

                if (callbacks.hasOwnProperty(data.url)) {
                    callbacks[data.url](downloaded[data.url].slice(0));
                }

                delete callbacks[data.url];
            } else {
                onair.log.info('Ignoring received file, download timeout ');
            }
        });
    });

    peer.on('open', function (id) {
        peerJsInitaliazed = true;
    });

    peer.on('error', function (err) {
        if (err.type === 'network') {
            peerJsInitaliazed = false;
        }
    });

    peer.on('close', function () {
        onair.log.info('Peer close');
    });

    // init sockjs
    var sock = new SockJS("http://" + hostname + ":" + portSockjs + "/sockjs", undefined, {transports: ['websocket']});

    sock.onopen = function () {
        sockJsInitaliazed = true;
    };

    sock.onclose = function () {
        sockJsInitaliazed = false;
    };

    sock.onmessage = function (e) {
        var message = JSON.parse(e.data);
        if ('REQUEST' === message.type) {
            var conn = peer.connect(message.peerId, {reliable: true});
            conn.on('open', function () { // send segments to remote peer
                peerSentBandwith += downloaded[message.url].byteLength; // update statistic
                conn.send({segment: downloaded[message.url], url: message.url});  // файл илгээнэ
            });
        }
    };

    // relase memory
    var release = function () {
        // 90 secondoo umnux iig ustgana
        var time = new Date().getTime() - 1000 * 60;
        for (var url in urls) {
            if (urls[url] < time) {
                delete urls[url];

                // timeouts
                if (fileReceiveTimeouts.hasOwnProperty(url)) {
                    delete fileReceiveTimeouts[url];
                }
                // downloaded
                if (downloaded.hasOwnProperty(url)) {
                    delete downloaded[url];
                }
                // callbacks
                if (callbacks.hasOwnProperty(url)) {
                    delete callbacks[url];
                }
            }
        }
        // console.log(Object.keys(downloaded).length);
    };

    var normalizeUri = function (_url) {
        return _url.replace(/\?.+/, '').replace(/_w\d+_/g, '_');
    };

    var defaultTimeout = 5000;
    //
    window.setInterval(release, 10 * 1000);

    return {
        retreive: function (_url, successFn, fallbackFn) {
            if (peerJsInitaliazed === false || sockJsInitaliazed === false) {
                return fallbackFn();
            }

            // remove query string
            var url = normalizeUri(_url);

            urls[url] = new Date().getTime();

            callbacks[url] = successFn;

            // part retreive request
            // TODO calculate timeout value
            sock.send(JSON.stringify({
                type: 'RETREIVE',
                url: url,
                peerId: peerId
            }));

            //
            fileReceiveTimeouts[url] = window.setTimeout(function () {
                delete callbacks[url];
                delete fileReceiveTimeouts[url];

                fallbackFn();
            }, defaultTimeout);
        },
        downloaded: function (_url, response, peer) {
            //  statistical data
            if (peer === true) {
                peerReceivedBandwith += response.byteLength;
            } else {
                serverReceivedBandwith += response.byteLength;
            }

            if (peerJsInitaliazed === true || sockJsInitaliazed === true) {
                // remove query string
                var url = normalizeUri(_url);

                // clone arrayBuffer
                downloaded[url] = response;

                sock.send(JSON.stringify({
                    type: 'DOWNLOADED',
                    url: url,
                    peerId: peerId
                }));
            }

        },
        abort: function (_url) {
            var url = normalizeUri(_url);

            if (fileReceiveTimeouts.hasOwnProperty(url)) {
                window.clearTimeout(fileReceiveTimeouts[url]);
                delete fileReceiveTimeouts[url];
            }
        },
        stats: function () {
            return {
                'serverReceived': serverReceivedBandwith,
                'peerReceived': peerReceivedBandwith,
                'peerSent': peerSentBandwith
            };
        }
    };
})();


onair.xhr = function () {
    // default request
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.mediaSegment = false;
    xmlhttp.requestSent = false;

    // properties
    var _readyState = 0;
    var _status = 0;
    var _statusText = '';

    var _response;
    var _responseText;
    var _responseXML;
    var _responseURL;

    //
    var timeout;

    var _timeoutId;
    var _aborted = false;
    var _timedout = false;
    var _loadCalled = false;

    // variuos event listener

    // onerror
    var onerror = function () {};
    var _onerror = function () {
        window.clearTimeout(_timeoutId);
        _readyState = xmlhttp.readyState, _status = xmlhttp.status;
        
        onerror();
    };
    // onabort
    var onabort = function () {};
    var _onabort = function () {
        window.clearTimeout(_timeoutId);
        _readyState = xmlhttp.readyState, _status = xmlhttp.status, _aborted = true;
        onabort();
    };
    // ontimeout
    var ontimeout = function () {};

    var _ontimeout = function () {
        window.clearTimeout(_timeoutId);
        //
        _readyState = 4, _status = 0, _timedout = true;

        // TODO CHECK
        if (xmlhttp.requestSent) {
            xmlhttp.abort('timeout');

        } else {
            onair.peer.abort(xmlhttp.uri);  // abort
            // TODO check
            onreadystatechange();   // trigger onreadystatechange
        }
        ontimeout();
    };

    // onprogress
    var onprogress = function (event) {};
    var _onprogress = function (event) {
        _readyState = xmlhttp.readyState, _status = xmlhttp.status;
        onprogress(event);
    };
    // onload
    var onload = function () {};
    var _onload = function () {
        _readyState = xmlhttp.readyState, _status = xmlhttp.status;

        if (_readyState === 4 && _status === 200) {
            loadFunc();
        }
        onload();
    };

    // onready state change event listener
    var onreadystatechange = function () {};
    var _onreadystatechange = function () {
        _readyState = xmlhttp.readyState, _status = xmlhttp.status;

        if (_readyState === 4 && _status === 200) {
            loadFunc();
        }
        onreadystatechange();
    };


    // send section start here
    function loadFunc() {
        window.clearTimeout(_timeoutId); // clearTimeout

        if (_loadCalled === true) {
            return;
        }
        _loadCalled = true;

        if (xmlhttp.mediaSegment && xmlhttp.requestSent && xmlhttp.response) {
            onair.peer.downloaded(xmlhttp.uri, xmlhttp.response.slice(0), false);
        }
    }

    function _send(string) {
        xmlhttp.requestSent = true;
        xmlhttp.send(string);
    }

    var onFailedCallback = function () {
        if (_aborted === false && _timedout === false) {
            _send();
        }
    };

    var onSuccessCallback = function (response) {
        if (_aborted === false && _timedout === false) {
            _readyState = 4, _status = 200, _response = response;

            onload();
        }
    };

    //
    // functions
    // 

    this.getAllResponseHeaders = function () {
        return xmlhttp.getAllResponseHeaders();
    };

    this.getResponseHeader = function () {
        return xmlhttp.getResponseHeader();
    };

    this.open = function (_method, _url, _async, _user, _psw) {
        _readyState = 1, _status = 0;
        // 
        xmlhttp.uri = _url;

        //
        xmlhttp.mediaSegment = xmlhttp.uri.match(/\d+\.ts/) ? true : false;

        // 
        xmlhttp.open(_method, _url, _async, _user, _psw);
    };

    // send
    this.send = function (_string) {
        _readyState = 2, _status = 0;

        // has timeout
        if (typeof timeout === 'number' && timeout > 0) {
            _timeoutId = window.setTimeout(function () {
                _ontimeout();
            }, timeout);
        }

        // is media segment request
        if (xmlhttp.mediaSegment) {
            onair.peer.retreive(xmlhttp.uri, onSuccessCallback, onFailedCallback);
        } else {
            _send(_string);
        }
    };

    this.abort = function () {
        _aborted = true;
        
        if (xmlhttp.requestSent) {
            xmlhttp.abort('timeout');
        } else {
            onair.peer.abort(xmlhttp.uri);
      
            onreadystatechange();
      
            onabort();
        }
    };


    this.setRequestHeader = function (header, value) {
        xmlhttp.setRequestHeader(header, value);
    };


    //_responseType
    this.addEventListener = function (type, listener) {
        if (type === 'progress') {
            onprogress = listener;
            xmlhttp.addEventListener(type, _onprogress);
        } else {
            xmlhttp.addEventListener(type, listener);
        }
    };


    // properties 

    // onreadystatechange
    Object.defineProperty(this, 'onreadystatechange', {
        get: function () {
            return onreadystatechange;
        },
        set: function (value) {
            onreadystatechange = value;
            xmlhttp.onreadystatechange = _onreadystatechange;
        }
    });


    // withCredentials
    Object.defineProperty(this, 'withCredentials', {
        get: function () {
            return xmlhttp.withCredentials;
        },
        set: function (value) {
            xmlhttp.withCredentials = value;
        }
    });

    // readyState
    Object.defineProperty(this, 'readyState', {
        get: function () {
            return xmlhttp.requestSent ? xmlhttp.readyState : _readyState;
        }
    });

    //_response
    Object.defineProperty(this, 'response', {
        get: function () {
            return xmlhttp.requestSent ? xmlhttp.response : _response;
        }
    });

    //_responseText
    Object.defineProperty(this, 'responseText', {
        get: function () {
            return xmlhttp.requestSent ? xmlhttp.responseText : _responseText;
        }
    });

    //_responseType
    Object.defineProperty(this, 'responseType', {
        get: function () {
            return xmlhttp.responseType;
        },
        set: function (value) {
            xmlhttp.responseType = value;
        }
    });

    // timeout
    Object.defineProperty(this, 'timeout', {
        get: function () {
            return timeout;
        },
        set: function (value) {
            timeout = value;
        }
    });

    //_response url
    Object.defineProperty(this, 'responseURL', {
        get: function () {
            return xmlhttp.requestSent ? xmlhttp.responseURL : _responseURL;
        }
    });

    //_response url
    Object.defineProperty(this, 'responseXML', {
        get: function () {
            return xmlhttp.requestSent ? xmlhttp.responseXML : _responseXML;
        }
    });

    // status
    Object.defineProperty(this, 'status', {
        get: function () {
            return xmlhttp.requestSent ? xmlhttp.status : _status;
        }
    });

    // statusText 
    Object.defineProperty(this, 'statusText', {
        get: function () {
            return xmlhttp.requestSent ? xmlhttp.statusText : _statusText;
        }
    });
    // variuos callback function

    // onload
    Object.defineProperty(this, 'onload', {
        get: function () {
            return onload;
        },
        set: function (value) {
            onload = value;
            xmlhttp.onload = _onload;
        }
    });

    // onerror
    Object.defineProperty(this, 'onerror', {
        get: function () {
            return onerror;
        },
        set: function (value) {
            onerror = value;
            xmlhttp.onerror = _onerror;
        }
    });

    // onabort
    Object.defineProperty(this, 'onabort', {
        get: function () {
            return onabort;
        },
        set: function (value) {
            onabort = value;
            xmlhttp.onabort = _onabort;
        }
    });

    // ontimeout
    Object.defineProperty(this, 'ontimeout', {
        get: function () {
            return ontimeout;
        },
        set: function (value) {
            ontimeout = value;
        }
    });

    return this;
};