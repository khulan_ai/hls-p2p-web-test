function bytesToHuman(x) {
  var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
          n = parseInt(x, 10) || 0,
          l = 0;
  while (n >= 1024) {
    n = n / 1024;
    l++;
  }
  return(n.toFixed(n >= 10 || l < 1 ? 0 : 1) + ' ' + units[l]);
}

// debugger
var debug = {
	level: 0, // 1 warning only, 2 warning and info
	warn: function(msg){
		if (debug.level >= 1){
			console.warn(msg);
		}
	},
	info: function(msg){
		if (debug.level >= 2){
			console.info(msg);
		}
	}
}

var m3u8Bandwith = 0;
var peerReceivedBandwith = 0;
var peerSentBandwith = 0;


var portSockjs = '3000';
var portPeerjs = '9000';
var hostname = location.hostname;
var prefix = 'sockjs';


// downloaded segments 
var segments = {};
var activeCallbacks = {};
var urlMaps = {};
// file receive max timeout
var maxWaitTimeout = 5000;
// file receive min timeout
var minWaitTimeout = 1000;

// init peerjs
var peerId = Math.floor(Math.random() * (99999999 - 10000000)) + 10000000;
debug.info("Peer Id:" + peerId);

var peer = new Peer(peerId, {host: hostname, port: portPeerjs});




// файл хэн нэгнээс ирэх үед
peer.on('connection', function (conn) {
  conn.on('data', function (data) {
    // hash
    var hash = asmCrypto.SHA256.hex(data);

    debug.warn("File received hash: " + hash);

    if (urlMaps.hasOwnProperty(hash)) {
      var url = urlMaps[hash];
      segments[url] = data.slice(0);
      debug.info("Event part downloaded: " + url);

      // part downloaded request
      sock.send(JSON.stringify({
        type: 'SEGMENT_DOWNLOADED',
        url: url,
        hash: hash
      }));

    } else {
      debug.warn("Hasn not found: " + hash);
    }
  });
});



// init sockjs
var sock = new SockJS("http://" + hostname + ":" + portSockjs + "/" + prefix);
sock.onopen = function () {
  debug.info("SockJS server connected");
};
sock.onmessage = function (e) {
  var message = JSON.parse(e.data);
  switch (message.type) {
    case 'SEGMENT_REQUEST': // парт авах хүсэлт хэн нэгнээс ирсэн
      // парт авах хүсэлт илгээсн хүн рүү файл илгээнэ
      debug.warn("Sending segments to remote peer id:" + message.peerId);
      var conn = peer.connect(message.peerId, {reliable: true});
      // send segments to remote peer
      conn.on('open', function () {
        // update statistic
        peerSentBandwith += segments[message.url].slice(0).byteLength;

        document.getElementById('peer_sent_bandwith').innerHTML = bytesToHuman(peerSentBandwith);

        // send file
        conn.send(segments[message.url].slice(0));
      });
      break;

    case 'SEGMENT_INFO':
      // парт мэдээллийг илгээнэ
      debug.warn("Event received part info" + e.data);
      if (message.hash) {
        urlMaps[message.hash] = message.url;
      }
      // callback
      activeCallbacks[message.callbackId](message.length);

      break;

  }
};

sock.onclose = function () {
  debug.info("close");
};


function customLoader() {
  var self = this;


  function xhrPartDownload(context, config, callbacks) {
    debug.info("Making xhr request");
    // XML request
    self.loader = new XMLHttpRequest();
    self.loader.open('GET', context.url);
    self.loader.responseType = context.responseType;
    if (context.rangeStart && context.rangeEnd) {
      xhr.setRequestHeader('Range', 'bytes=' + context.rangeStart + '-' + context.rangeEnd);
    }
    self.loader.send();

    // ajax onload
    self.loader.onload = function (xhr) {
      // remove query string
      var url = context.url.replace(/\?.+/, "");
      url = url.replace(/_w\d+_/g, '_');

      debug.info("Downloaded fragment from m3u8 url:" + url);

      // clone arrayBuffer
      segments[url] = xhr.target.response.slice(0);

      debug.info("Event part downloaded: " + url);

      sock.send(JSON.stringify({
        type: 'SEGMENT_DOWNLOADED',
        url: url,
        hash: asmCrypto.SHA256.hex(segments[url])
      }));


      m3u8Bandwith += segments[url].byteLength;
//
      document.getElementById('m3u8_bandwith').innerHTML = bytesToHuman(m3u8Bandwith);

      // callback
      callbacks.onSuccess({data: xhr.target.response}, {}, context);
    };
    self.loader.onprogress = function (xhr) {
      callbacks.onProgress({}, context, context.progressData === true ? xhr.target.response : undefined);
    };
  }

  this.load = function (context, config, callbacks) {
    debug.info("---- HLS load called ----");
    debug.info("Original url: " + context.url);

    // url
    var url = context.url.replace(/\?.+/, "");
    url = url.replace(/_w\d+_/g, '_');

    debug.info("Event part retreive:" + url);

    var callbackId = Math.floor(Math.random() * (99999999 - 10000000)) + 10000000;

    // callback 
    activeCallbacks[callbackId] = function (length) {
      if (length > 0) {
        debug.warn('Waiting for sending file');
        setTimeout(function () {
          // is downloaded
          if (segments.hasOwnProperty(url)) {
            debug.warn('Using p2p file');

            // received bandwith
            peerReceivedBandwith += segments[url].slice(0).byteLength;

            document.getElementById('peer_received_bandwith').innerHTML = bytesToHuman(peerReceivedBandwith);

            callbacks.onSuccess({data: segments[url].slice(0)}, {}, context);
          } else {
            debug.info('Using xhr loader');
            xhrPartDownload(context, config, callbacks);
          }
        }, Math.floor(Math.random() * (maxWaitTimeout - minWaitTimeout)) + minWaitTimeout);

      } else {
        debug.info('Using xhr loader');
        // send xhr retreive request
        xhrPartDownload(context, config, callbacks);
      }
      // 
      delete activeCallbacks[callbackId];
    };

    // part retreive request
    sock.send(JSON.stringify({
      type: 'SEGMENT_RETREIVE',
      url: url,
      peerId: peerId,
      callbackId: callbackId
    }));

  };
  this.abort = function () {
    // TODO abort function
    // this.loader.abort();
    debug.info('abort called called');
  };
  this.destroy = function () {
    //
    debug.info('Destroy called');
  };
}

